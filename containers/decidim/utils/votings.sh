#!/bin/bash
sudo -u decidim bash <<EOF
source ~/.bashrc
cd /app/lazydecidim
echo "gem 'decidim-votings'" | tee -a Gemfile
bundle
EOF
