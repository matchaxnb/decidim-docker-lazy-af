#!/bin/bash
sudo -u decidim bash <<EOF
source ~/.bashrc
cd /app/lazydecidim
echo "gem 'decidim-participatory_processes'" | tee -a Gemfile
bundle
EOF
