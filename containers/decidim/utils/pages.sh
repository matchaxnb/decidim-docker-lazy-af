#!/bin/bash
sudo -u decidim bash <<EOF
source ~/.bashrc
cd /app/lazydecidim
echo "gem 'decidim-pages'" | tee -a Gemfile
bundle
EOF
