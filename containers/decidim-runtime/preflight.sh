#!/bin/bash
set -x

export RAILS_ENV=production
D_LOGIN=${DECIDIM_DEFAULT_LOGIN}
D_PASSWORD=${DECIDIM_DEFAULT_PASSWORD}


chown -R decidim:decidim /app/*
cd /app/lazydecidim
if [ "$DECIDIM_FORCE_CREATE_REGEN_CONFIG" = "yes" ]; then
    SKIPCFG=1
else
    grep '#BERN_SETUP_ALREADY' config/application.yml >/dev/null 2>&1
    SKIPCFG=$?
fi

if [ ! -f /app/lazydecidim/config/.db_created -o $SKIPCFG != 0 ]; then
    RAKESECRET=$(sudo -u decidim bash -c 'source /app/.bashrc; cd /app/lazydecidim; rake secret')
    cat > config/application.yml <<EOC
DATABASE_URL: "postgres://${DECIDIM_PG_USER}:${DECIDIM_PG_PASSWD}@${DECIDIM_PG_HOST}:${DECIDIM_PG_PORT}/${DECIDIM_PG_DB}"
SECRET_KEY_BASE: $RAKESECRET
#BERN_SETUP_ALREADY
EOC
chown decidim:decidim config/application.yml

sudo -u decidim bash - <<EOG
source /app/.bashrc
export RAILS_ENV=production
# run an initializer if present
touch /app/decidim-config.sh
bash /app/decidim-config.sh
# initialize db and stuff now
cd /app/lazydecidim
sed -i -e 's@smtp_port: "587"@<%= ENV["SMTP_PORT"] || "587" %>@g' config/secrets.yml
bin/rails db:create RAILS_ENV=production
EOG

sudo -u decidim bash - <<EOG
source /app/.bashrc
export RAILS_ENV=production
# pre-build assets
ls /app/lazydecidim/public/assets/*js.gz || bin/rails assets:precompile  RAILS_ENV=production
# create a default user
EOG

sudo -u decidim bash - <<EOG
source /app/.bashrc
export RAILS_ENV=production
cat > /tmp/user_tmp.rb <<EOF
email = "$D_LOGIN"
password = "$D_PASSWORD"
user = Decidim::System::Admin.new(email: email, password: password, password_confirmation: password)
user.save!
EOF

bin/rails runner /tmp/user_tmp.rb
EOG


sudo -u decidim bash - <<EOG
source /app/.bashrc
export RAILS_ENV=production
bin/rails generate delayed_job:active_record
bin/rails db:migrate RAILS_ENV=production
EOG
touch /app/lazydecidim/config/.db_created
fi

chown -R decidim:decidim /app/*
# run inconditionally before app start
if [ -f /app/before-startup.sh ]; then
    bash /app/before-startup.sh
fi

test -f /app/environment.sh && . /app/environment.sh
